//Standard PWM DC control
int E1 = 5;     //M1 Speed Control
int E2 = 6;     //M2 Speed Control
int M1 = 4;    //M1 Direction Control
int M2 = 7;    //M1 Direction Control
 
///For previous Romeo, please use these pins.
//int E1 = 6;     //M1 Speed Control
//int E2 = 9;     //M2 Speed Control
//int M1 = 7;    //M1 Direction Control
//int M2 = 8;    //M1 Direction Control
 
// Sensors
int SPD1 = 12;
int SPD2 = 13;
int LNE_L = 2;
int LNE_R = 3;
 
void stop(void)                    //Stop
{
  digitalWrite(E1,LOW);   
  digitalWrite(E2,LOW);      
}   
void advance(char a,char b)          //Move forward
{
  analogWrite (E1,a);      //PWM Speed Control
  digitalWrite(M1,HIGH);    
  analogWrite (E2,b);    
  digitalWrite(M2,HIGH);
}  
void back_off (char a,char b)          //Move backward
{
  analogWrite (E1,a);
  digitalWrite(M1,LOW);   
  analogWrite (E2,b);    
  digitalWrite(M2,LOW);
}
void turn_L (char a,char b)             //Turn Left
{
  analogWrite (E1,b);
  digitalWrite(M1,HIGH);    
  analogWrite (E2,a);    
  digitalWrite(M2,LOW);
}
void turn_R (char a,char b)             //Turn Right
{
  analogWrite (E1,b);
  digitalWrite(M1,LOW);    
  analogWrite (E2,a);    
  digitalWrite(M2,HIGH);
}
void setup(void) 
{ 
  int i;
  for(i=4;i<=7;i++)
    pinMode(i, OUTPUT);  
  pinMode(10,INPUT);
  pinMode(12, INPUT);
  pinMode(13,INPUT);
  Serial.begin(9600);      //Set Baud Rate
  Serial1.begin(9600);
  Serial.println("Run keyboard control");
} 
void loop(void) 
{
Serial.print("Left: ");
Serial.println(digitalRead(LNE_L));
Serial.print("Right: ");
Serial.println(digitalRead(LNE_R));
delay(500);
}
