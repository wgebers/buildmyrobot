//Standard PWM DC control
int E1 = 5;     //M1 Speed Control
int E2 = 6;     //M2 Speed Control
int M1 = 4;    //M1 Direction Control
int M2 = 7;    //M1 Direction Control
 
///For previous Romeo, please use these pins.
//int E1 = 6;     //M1 Speed Control
//int E2 = 9;     //M2 Speed Control
//int M1 = 7;    //M1 Direction Control
//int M2 = 8;    //M1 Direction Control
 
// Sensors
int SPD1 = 12;
int SPD2 = 13;
int LNE_L = 0;
int LNE_M = 1;
int LNE_R = 2;


int left = 100;
int right = 100;
 
void stop(void)                    //Stop
{
  digitalWrite(E1,LOW);   
  digitalWrite(E2,LOW);      
}   
void advance(char a,char b)          //Move forward
{
  analogWrite (E1,a);      //PWM Speed Control
  digitalWrite(M1,HIGH);    
  analogWrite (E2,b);    
  digitalWrite(M2,HIGH);
}  
void back_off (char a,char b)          //Move backward
{
  analogWrite (E1,a);
  digitalWrite(M1,LOW);   
  analogWrite (E2,b);    
  digitalWrite(M2,LOW);
}
void turn_L (char a,char b)             //Turn Left
{
  analogWrite (E1,a);
  digitalWrite(M1,LOW);    
  analogWrite (E2,b);    
  digitalWrite(M2,HIGH);
}
void turn_R (char a,char b)             //Turn Right
{
  analogWrite (E1,a);
  digitalWrite(M1,HIGH);    
  analogWrite (E2,b);    
  digitalWrite(M2,LOW);
}
void setup(void) 
{ 
  int i;
  for(i=4;i<=7;i++)
    pinMode(i, OUTPUT);  
  pinMode(10,INPUT);
  pinMode(12, INPUT);
  pinMode(13,INPUT);
  pinMode(LNE_L, INPUT);
  pinMode(LNE_M, INPUT);
  pinMode(LNE_R, INPUT);
  Serial.begin(19200);      //Set Baud Rate
  Serial.println("Run keyboard control");
} 

void run() {
  boolean l = !(digitalRead(LNE_L));
  boolean m = !(digitalRead(LNE_M));
  boolean r = !(digitalRead(LNE_R));

//  Serial.print("Run");
//  Serial.print(l);
//  Serial.print(m);
//  Serial.println(r);
  
  if (m && !l && !r) {
     advance(100, 100);
  } else if (l && !r) {
     advance(50, 100);
  } else if (!l && r) {
     advance(100, 50);
  } else {
     advance(100,100); 
  }
}



void loop(void) 
{
  //  if(false && Serial.available()){
  //   char val = Serial.read();
  //   Serial.println(digitalRead(LNE_L));
  //   if(val != -1)
  //   {
  //     switch(val)
  //     {
  //     case 'w'://Move Forward
  //       advance (100,100);   //move forward in max speed
  //       break;
  //     case 's'://Move Backward
  //       back_off (100,100);   //move back in max speed
  //       break;
  //     case 'a'://Turn Left
  //       turn_L (100,100);
  //       break;       
  //     case 'd'://Turn Right
  //       turn_R (100,100);
  //       break;
  //     case 'z':
  //       Serial.print("Hello");
  //       Serial.print(digitalRead(LNE_L));
  //       Serial.print(digitalRead(LNE_M));
  //       Serial.print(digitalRead(LNE_R));
  //       Serial.println("");
  //       break;
  //     case 'x':
  //       stop();
  //       break;
  //     }
  //   }    
  // } 
  // else 
  // { 
    run(); 
  // }

}
