// First basic script to make the robot follow a line. 

// Remember to set the board to an Arduino Leonardo type in the Arduino environment
// Check that the correct serial port is selected under Tools
// The motors require an analogue value of 75 minimum to start moving. 

// Romeo V2.0 Board
int M1_SPEED = 5;                    //M1 Speed Control (0-255) connected to pin 5
int M2_SPEED = 6;                    //M2 Speed Control (0-255) connected to pin 6
int M1_DIRECTION = 4;                //M1 Direction Control (0/1) connected to pin 4
int M2_DIRECTION = 7;                //M1 Direction Control (0/1  connected to pin 7
// NB!  These pins are used for the motor drive control therefore can't be used for anything else!

// Sensors
int LINE_SENSOR_LEFT = 10;           // Connected to Digital input 10
int LINE_SENSOR_RIGHT = 9;           // Connected to Digital input 9
 
void stop(void)                      //Stop
{
  digitalWrite(M1_SPEED,0);   
  digitalWrite(M2_SPEED,0);      
}   
void advance(char a,char b)          //Move forward
{
  digitalWrite(M1_DIRECTION,HIGH);   // Set Motor 1 direction forward
  analogWrite (M1_SPEED,a);          // Set Motor 1 speed
  digitalWrite(M2_DIRECTION,HIGH);   // Set Motor 2 direction forward
  analogWrite (M2_SPEED,b);          // Set Motor 2 speed
}  
void back_off (char a,char b)          //Move backward
{
  digitalWrite(M1_DIRECTION,LOW);    // Set Motor 1 direction backwards
  analogWrite (M1_SPEED,a);          // Set Motor 1 speed
  digitalWrite(M2_DIRECTION,LOW);    // Set Motor 2 direction backwards
  analogWrite (M2_SPEED,b);          // Set Motor 2 speed

}
void turn_Left (char a,char b)             //Turn Left
{
  digitalWrite(M1_DIRECTION,LOW);       // Set Motor 1 Direction Backwards
  analogWrite (M1_SPEED,a);             // Set Motor 1 Speed
  digitalWrite(M2_DIRECTION,HIGH);      // Set Motor 2 Direction Forwards
  analogWrite (M2_SPEED,b);             // Set Motor 2 Speed

}
void turn_Right (char a,char b)             //Turn Right
{
  analogWrite (M1_SPEED,a);             // Set Motor 1 Speed
  digitalWrite(M1_DIRECTION,HIGH);      // Set Motor 1 Direction Forwards
  analogWrite (M2_SPEED,b);             // Set Motor 2 Speed
  digitalWrite(M2_DIRECTION,LOW);       // Set Motor 2 Direction Backwards
}

// The setup routine runs when the micro controller starts up
void setup(void) 
{ 
  // Configure the pins for first use
  // Configure drive pins as output
  pinMode(M1_SPEED, OUTPUT);           
  pinMode(M2_SPEED, OUTPUT);
  pinMode(M1_DIRECTION, OUTPUT);
  pinMode(M2_DIRECTION, OUTPUT);
  // Configure sensor pins as input
  pinMode(LINE_SENSOR_LEFT,INPUT);
  pinMode(LINE_SENSOR_RIGHT, INPUT);
  
  Serial.begin(9600);      //Set Baud Rate
  Serial.println("Run keyboard control");
} 

// The main loop that runs continuously
void loop(void) 
{

  // If the left line sensor sees the white line, turn left to get back on track
  if(digitalRead(LINE_SENSOR_LEFT))
  {
    turn_Left(100,100);   
  }
 // If the right line sensor seees the white line, turn right to get back on track
  if(digitalRead(LINE_SENSOR_RIGHT))
  {
    turn_Right(100,100);
  }
  // If we aren't seeing, the line, we must be on the line, so full speed ahead. 
  if(!digitalRead(LINE_SENSOR_LEFT)&&!digitalRead(LINE_SENSOR_RIGHT))
  {
    advance(150,150);
  }
}
