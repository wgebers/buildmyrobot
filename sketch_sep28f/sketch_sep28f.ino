//Standard PWM DC control
int E1 = 5;     //M1 Speed Control
int E2 = 6;     //M2 Speed Control
int M1 = 4;    //M1 Direction Control
int M2 = 7;    //M1 Direction Control
 
///For previous Romeo, please use these pins.
//int E1 = 6;     //M1 Speed Control
//int E2 = 9;     //M2 Speed Control
//int M1 = 7;    //M1 Direction Control
//int M2 = 8;    //M1 Direction Control
 
// Sensors
int SPD1 = 12;
int SPD2 = 13;
int LNE_L = 10;
int LNE_R = 9;
 
void stop(void)                    //Stop
{
  digitalWrite(E1,LOW);   
  digitalWrite(E2,LOW);      
}   
void advance(char a,char b)          //Move forward
{
  analogWrite (E1,a);      //PWM Speed Control
  digitalWrite(M1,HIGH);    
  analogWrite (E2,b);    
  digitalWrite(M2,HIGH);
}  
void back_off (char a,char b)          //Move backward
{
  analogWrite (E1,a);
  digitalWrite(M1,LOW);   
  analogWrite (E2,b);    
  digitalWrite(M2,LOW);
}
void turn_R (char a,char b)             //Turn Left
{
  analogWrite (E1,a);
  digitalWrite(M1,LOW);    
  analogWrite (E2,b);    
  digitalWrite(M2,HIGH);
}
void turn_L (char a,char b)             //Turn Right
{
  analogWrite (E1,a);
  digitalWrite(M1,HIGH);    
  analogWrite (E2,b);    
  digitalWrite(M2,LOW);
}
void setup(void) 
{ 
  int i;
  for(i=4;i<=7;i++)
    pinMode(i, OUTPUT);  
  pinMode(10,INPUT);
  pinMode(12, INPUT);
  pinMode(13,INPUT);
  Serial1.begin(9600);      //Set Baud Rate
  Serial1.println("Run keyboard control");
} 
void loop(void) 
{
   if(Serial1.available()){
    char val = Serial1.read();
    if(val != -1)
    {
      switch(val)
      {
      case 'w'://Move Forward
        advance (200,200);   //move forward in max speed
        break;
      case 's'://Move Backward
        back_off (100,100);   //move back in max speed
        break;
      case 'a'://Turn Left
        turn_L (75,75);
        break;       
      case 'd'://Turn Right
        turn_R (75,75);
        break;
      case 'z':
        Serial1.println("Hello");
        break;
      case 'x':
        stop();
        break;
      }
    }
    else stop();  
   
    
  } 
}
